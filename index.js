
/**
 * bài 1
 * input:
 *  nhập 3 số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo biến thisNgay, thisThang, thisNam, result
 * b2: kiểm tra biến thisNam người dùng nhập vào có hợp lệ hay ko
 * b3: kiểm tra biến thisThang người dùng nhập vào có hợp lệ hay ko
 * b4: kiểm tra biến thisNgay người dùng nhập vào có hợp lệ hay ko
 *  nếu nếu hợp lệ hết thì
 * b5.1 ngày hôm qua: 
 *           nếu ngày 1 của các tháng 5, 7, 10, 12 => thisNgay == 30, thisThang--;
 *           nếu ngày 1 của các tháng 1, 2, 4,6, 8, 9, 11 => thisNgay == 31, thisThang--;
 *                  nếu ngày 1 của tháng 1 => thisNgay == 31, thisThang = 12, thisNam--;
 *           nếu ngày 1 của tháng 3 của năm nhuận  => thisNgay == 29, thisThang--;
 *           nếu ngày 1 của tháng 3 của năm ko nhuận  => thisNgay == 28, thisThang--;
 *           ngược lại thisNgay--;
 * 
 *  b5.2 ngày mai:
 *           nếu ngày 31 của các tháng 1, 3, 5, 7, 8, 10, 12 => thisNgay == 1, thisThang++;
 *                  nếu ngày 31 của tháng 12 => thisNgay == 1, thisThang = 12;
 *           nếu ngày 30 của các tháng 4,6, 9, 11 => thisNgay == 1, thisThang++;
 *           nếu ngày 29 của tháng 2 của năm nhuận  => thisNgay == 1, thisThang++;
 *           nếu ngày 28 của tháng 2 của năm ko nhuận  => thisNgay == 1, thisThang++;
 *           ngược lại thisNgay++;
 *  
 * b6: in kết quả ngày tháng năm sau khi tính bằng innerHTML
 * 
 * output:
 *  kết quả ngày sau khi tính.
 */
var result = document.getElementById("result");
function homQua(){
    let thisNgay = document.getElementById("ngay-Nhap").value*1;
    let thisThang = document.getElementById("thang-Nhap").value*1;
    let thisNam = document.getElementById("nam-Nhap").value*1;
    if(thisNam < 1919 || thisNam > 9999 ){
            alert("Năm phải lớn hơn 1920");
    }
    else{ 
        //thoả mãn năm
        if(thisThang < 1 || thisThang > 12){
            alert("Tháng phải nhỏ hơn hoặc bằng 12");
        }
        else{
            //thoả mãn năm && thoả nãm tháng
            if(thisNgay > 31 || thisNgay < 1){
                alert("1 tháng chỉ có tói đa 31 ngày");
            }
            else{
                //thoả mãn ngày, tháng, năm
                if( thisNgay == 1 && (thisThang == 5 || thisThang == 7 || thisThang == 10 || thisThang == 12)){
                    thisNgay = 30;
                    thisThang--;
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
                else if( thisNgay == 1 && (thisThang == 1 || thisThang == 2 || thisThang == 4 || thisThang == 6 || thisThang == 8 || thisThang == 9 || thisThang == 11)){
                    if(thisThang ==1){
                        thisNgay = 31;
                        thisThang = 12;
                        thisNam--;
                    }
                    else{
                        thisNgay = 31;
                        thisThang--;
                    }
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
                else if(thisNgay == 1 && thisThang == 3){
                    if((thisNam%4 == 0 && thisNam%100 != 0) || thisNam%400 == 0){
                        thisNgay = 29;
                        thisThang--;
                    }
                    else {
                        thisNgay = 28;
                        thisThang--;
                    }
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
                else if(thisThang ==2){
                    if((thisNam%4 == 0 && thisNam%100 != 0) || thisNam%400 == 0){
                        if(thisNgay > 29){
                            alert("Tháng 2 chỉ có tối đa 29 ngày");
                        }
                        else{
                            thisNgay--;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                        }
                    }
                    else {
                        if(thisNgay > 28){
                            alert("Tháng 2 của năm không nhuận chỉ có tối đa 28 ngày");
                            console.log(13)
                        }
                        else{
                            thisNgay--;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                        }
                        
                    }
                }
                else {
                    thisNgay--;
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
            }
        }
    }
}

function ngayMai(){
    let thisNgay = document.getElementById("ngay-Nhap").value*1;
    let thisThang = document.getElementById("thang-Nhap").value*1;
    let thisNam = document.getElementById("nam-Nhap").value*1;
    if(thisNam < 1919 || thisNam > 9999 ){
            alert("Năm phải lớn hơn 1920");
    }
    else{ 
        //thoả mãn năm
        if(thisThang < 1 || thisThang > 12){
            alert("Tháng phải nhỏ hơn hoặc bằng 12");
        }
        else{
            //thoả mãn năm && thoả nãm tháng
            if(thisNgay > 31 || thisNgay < 1){
                alert("1 tháng chỉ có tói đa 31 ngày");

            }
            else{
                //thoả mãn ngày, tháng, năm
                if( thisNgay == 31 && (thisThang == 1 || thisThang == 3 || thisThang == 5 || thisThang == 7 || thisThang == 8 || thisThang == 10 || thisThang == 12)){
                    if(thisThang == 12){
                        thisNgay = 1;
                        thisThang = 1;
                        thisNam++;
                    }
                    else{
                        thisNgay = 1;
                        thisThang++;
                    }
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
                else if( thisNgay == 30 && (thisThang == 4 || thisThang == 6 || thisThang == 9 || thisThang == 11)){
                    thisNgay = 1;
                    thisThang++;
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
                else if(thisThang ==2){
                    if((thisNam%4 == 0 && thisNam%100 != 0) || thisNam%400 == 0){
                        if(thisNgay >= 29){
                            thisNgay = 1;
                            thisThang++;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;

                        }
                        else{
                            thisNgay ++;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                        }
                    }
                    else {
                        if(thisNgay >= 28){
                            thisNgay = 1;
                            thisThang++;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                        }
                        else{
                            thisNgay++;
                            result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                        }
                        
                    }
                }
                else {
                    thisNgay++;
                    result.innerHTML = `<h3>${thisNgay}/${thisThang}/${thisNam}</h3>`;
                }
            }
        }
    }
}
/**
 * bài 2
    input:
 *  nhập 3 số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo biến thangB2, namB2, ngayB2;
 * b2: kiểm tra biến namB2 người dùng nhập vào có hợp lệ hay ko
 * b3: kiểm tra biến thangB2 người dùng nhập vào có hợp lệ hay ko
 *  nếu nếu hợp lệ hết thì
 * b4:  nếu tháng bằng một trong các tháng 1,3,5,7,8,10,12 thì ngayB2 = 31;
 *      nếu tháng bằng một trong các tháng 4,6,9,11 thì ngayB2 = 30;
 *      ngược lại là tháng 2 thì:
 *                  nếu là năm nhuận thì ngayB2 = 29;
 *                  nếu kop hải năm nhuận thì ngayB2 = 28;
 * 
 * b5: in kết quả số ngày trong 1 tháng đó bằng innerHTML
 * 
 * output:
 *  kết quả ngày sau khi tính.
 */
function tinhNgayB2(){
    var thangB2 =document.getElementById("thang-B2").value*1;
    var namB2 =document.getElementById("nam-B2").value*1;
    var ngayB2;
    if(namB2 < 1919 || namB2 > 9999 ){
        alert("Năm phải lớn hơn 1920 và nhỏ hơn 9999");
    }
    else {
        if(thangB2 < 1 || thangB2 > 12){
            alert("Tháng phải nhỏ hơn hoặc bằng 12");
        }
        else {
            if(thangB2 == 1 || thangB2 == 3 || thangB2 == 5 || thangB2 == 7 || thangB2 == 8 || thangB2 == 10 || thangB2 == 12){
                ngayB2 = 31;
            }
            else if (thangB2 == 4 || thangB2 == 6 || thangB2 == 9 || thangB2 == 11){
                ngayB2 = 30;
            }
            else {
                if((namB2%4 == 0 && namB2%100 != 0) || namB2%400 == 0){
                    ngayB2 = 29;
                }
                else {
                    ngayB2 = 28
                }
            }
            result.innerHTML = `<h3>Tháng ${thangB2} năm ${namB2}  có ${ngayB2} ngày</h3>`;
        }
    }


}
/**
 * bài 3
 * input:d
 * nhập số có 3 chữ số từ bàn phím
 * 
 * bước xử lý
 * b1: tạo mảng chuSo, biến docSo, hangTram, hangChuc, donVi;
 * b2: tách hangTram = Math.floor(docSo/100);
            hangChuc = Math.floor(docSo%100/10);
            donVi = docSo%10;
 * b3:  nếu docSo nhỏ hon 100 hoặc lớn hơn 999 thì hiện số ko hợp lệ
        nếu ko thì:
                - nếu hàng chục = 0 và hàng dơn vị bằng 0 thì: 
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        (chỉ đọc mình hàng trăm)
                - nếu hàng chục = 0 và hàng dơn vị khác 0 thì: 
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        hàng dơn vị tương ứng thứ tự trong mảng chuSo
                        (đọc hàng trăm linh đơn vị)
                - nếu hàng chục = 1 và hàng dơn vị bằng 0 thì: 
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        (đọc hàng trăm + 'mười')
                - nếu hàng chục = 1 và hàng dơn vị khác 0 thì: 
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        hàng dơn vị tương ứng thứ tự trong mảng chuSo
                        (đọc hàng trăm + 'mười' + hàng đơn vị)
                - nếu hàng chục khác 0 và khác 1 và hàng đơn vị bằng 0 thì:
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        hàng chục bằng tương ứng thứ tự trong mảng chuSo 
                        (đọc hàng trăm + đọc hàng chục + 'mươi')
                - nếu hàng chục khác 0 và khác 1 và hàng đơn vị khác 0 thì:
                        hàng trăm bằng tương ứng thứ tự trong mảng chuSo
                        hàng chục bằng tương ứng thứ tự trong mảng chuSo 
                        hàng dơn vị bằng tương ứng thứ tự trong mảng chuSo 
                        (đọc hàng trăm + đọc hàng chục + 'mươi' + đọc đơn vị)

 * b5: in kết  bằng innerHTML
 * 
 * output:
 *  kết quả số đọc bằng chữ;
 */
function docSo(){
    var chuSo = [" không "," một "," hai "," ba "," bốn "," năm "," sáu "," bảy "," tám "," chín "];
    var docSo = document.getElementById('doc-so').value*1;
    var hangTram, hangChuc, donVi;
    for(var i = 0; i < chuSo.length; i++){
        console.log(chuSo[i]);
    }
    hangTram = Math.floor(docSo/100);
    hangChuc = Math.floor(docSo%100/10);
    donVi = docSo%10;
    if(docSo < 100 || docSo > 999){
        alert("Số không hợp lệ!")
    }
    else{
        if(hangChuc == 0){
            if(donVi == 0){
                hangTram = chuSo[hangTram];
                result.innerHTML = `<h3> ${hangTram} trăm</h3>`;
            }
            else {
                hangTram = chuSo[hangTram];
                donVi = chuSo[donVi];
                result.innerHTML = `<h3> ${hangTram} trăm linh ${donVi}</h3>`;
            }
        }
        else if(hangChuc == 1){
            if(donVi == 0){
                hangTram = chuSo[hangTram];
                result.innerHTML = `<h3> ${hangTram} trăm mười</h3>`;
            }
            else{
                hangTram = chuSo[hangTram];
                donVi = chuSo[donVi];
                result.innerHTML = `<h3> ${hangTram} trăm mười ${donVi}</h3>`;
            }
        }
        else{
            if(donVi == 0){
                hangTram = chuSo[hangTram];
                hangChuc = chuSo[hangChuc];
                result.innerHTML = `<h3> ${hangTram} trăm ${hangChuc} mươi</h3>`;
            }
            else{
                hangTram = chuSo[hangTram];
                hangChuc = chuSo[hangChuc];
                donVi = chuSo[donVi];
                result.innerHTML = `<h3> ${hangTram} trăm ${hangChuc} mươi ${donVi}</h3>`;
            }
        }
    }



}



/**
 * bài 4
 * input:
 *  nhập tên, toạ độ cho SV và trường
 *  
 * bước xử lý
 * b1: tạo biến nameSV1,nameSV2,nameSV3,
 *              xSV1, xSV2, xSV3, 
 *              ySV1, ySV2, ySV3,
 *              xSchool, ySchool
 * b2: gán giá trị cho các biến lấy từ input
 * b3: tạo và tính d1, d2, d3 dùng hàm sqrt và hàm pow
 *     để tính căn bậc 2 và tính luỹ thừa
 * b4: nếu d1 lơn hơn d2 và lơn hơn d3 thì in tên sv1
 *     nếu ko thì d2 lơn hơn d1 và lơn hơn d3 thì in tên sv2
 *     nngược lại thì in tên sv3
 * 
 * output:
 *  kết quả là sinh viên xa trường nhất
 */
//bai4
function tinhToaDo(){
    var nameSV1 = document.getElementById('name-sv1').value;
    var nameSV2 = document.getElementById('name-sv2').value;
    var nameSV3 = document.getElementById('name-sv3').value;
    var xSV1 = document.getElementById('x-sv1').value*1;
    var xSV2 = document.getElementById('x-sv2').value*1;
    var xSV3 = document.getElementById('x-sv3').value*1;
    var ySV1 = document.getElementById('y-sv1').value*1;
    var ySV2 = document.getElementById('y-sv2').value*1;
    var ySV3 = document.getElementById('y-sv3').value*1;
    var xSchool = document.getElementById('x-school').value*1;
    var ySchool = document.getElementById('y-school').value*1;
    
    var d1 = Math.sqrt(Math.pow((xSchool - xSV1),2) + Math.pow((ySchool - ySV1),2));
    var d2 = Math.sqrt(Math.pow((xSchool - xSV2),2) + Math.pow((ySchool - ySV2),2));
    var d3 = Math.sqrt(Math.pow((xSchool - xSV3),2) + Math.pow((ySchool - ySV3),2));
    if(d1 > d2 && d1 > d3){
        result.innerHTML = `<h3>Sinh viên xa trường nhất: ${nameSV1}</h3>`;
    }
    else if(d2 > d1 && d2 > d3) {
        result.innerHTML = `<h3>Sinh viên xa trường nhất: ${nameSV2}</h3>`;
    }
    else {
        result.innerHTML = `<h3>Sinh viên xa trường nhất: ${nameSV3}</h3>`;
    }
    
}



